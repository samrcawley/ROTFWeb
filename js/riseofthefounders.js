
var token;
var fadeArray = [];
var fadeTimeout;
var charid;
var charname;
///* Assists in cleaning string names for */
//String.prototype.clstr = function(){
//	if(typeof this != "string"){
//	return this;
//	}
//return this.toLowerCase().trim().split(" ")[0];
//};
//
//function cleanse(potato)
//{
//	return $(potato).children("span").text().clstr();
//}
function getTicketFromWebsite(){
	$.get("https://www.riseofthefounders.com/index.html", function(data){
			var doc = $.parseHTML(data);
			token = $(doc[0]).text();
			Auth();
		});
}

function activateUserMenu()
{
	"use strict";
	getResource("characters",function(payload){
		console.log(payload);
		charid = localStorage.getItem("charID");
		if (charid && payload.filter(function (i, d){return d.CharacterId === charid;}).length){
			charname = payload[0].CharacterName;
		}
		else{
			charid = payload[payload.length-1].CharacterId;
			charname = payload[payload.length - 1].CharacterName;
			localStorage.setItem("charID", charid);
		}
		
		$.each(payload, function(i, d){
			var activeStr = "";
			if(d.CharacterId === charid){ activeStr = "active";}
			$("#changechar .modal-body .list-group").append("<a class='list-group-item list-group-item-action " + activeStr +"' id='charlist-" + d.CharacterName + "' data-toggle='list' role='tab' aria-controls='home' onclick='listgroupselect(this);' data-charid='" + d.CharacterId + "'>" + d.CharacterName + "</a>");
		});
		
		$("#username").text(charname);
		$("#login").hide();
		$("#userdropdown").show();
	});
}

function deactivateUserMenu()
{
	"use strict";
	$("#username").text("");
	$("#userdropdown").hide();
	$("#login").show();
}

$(document).ready(function(){
	"use strict";
	$("#topmenu").load("./topmenu.html", function(){
		$("#loginform").submit(function(event){
		event.preventDefault();
		AuthScreen();
		});
		$("#passwordchange input[type=password]").keyup(function(){	
			if($("#pcinput1").val() !== $("#pcinput2").val()){
				$("#pwmatch").show();
			}else{
				$("#pwmatch").hide();
			}
			if($("#pcinput1").val().length <= 4){
				$("#pwshort").show();
			}else{
				$("#pwshort").hide();
			}
		});
	});
	$("#navmenu").load("./navmenu.html", function(){
		$("#navmenu .imgbox").each(function(index){
			var hrefval = $(this).attr("href");
			if(hrefval){
				$(this).click(function(){
					window.open(hrefval, '_blank');
				});
			}
		});
	});
	loadIntro();
	if(localStorage.getItem("Ticket")){
		token = localStorage.getItem("Ticket");
		Auth();
	}
	else{
		getTicketFromWebsite();
	}
	//console.log(localStorage.getItem("Ticket"));
	//token = localStorage.getItem("Ticket")||$("ticket").text();
	//Auth();
});

function loadMainBody(filename, callback)
{
	"use strict";
	clearInterval(fadeTimeout);
	$("#mainbody").load(filename, callback);
}

function loadDT(){
	"use strict";
	loadMainBody("./downtimes.html", getDowntimes);
	//clearInterval(fadeTimeout);
	//$("#mainbody").load("./downtimes.html", getDowntimes);
}

function loadschedule(){
	"use strict";
	loadMainBody("./schedule.html", function(){
		//get schedule data
		var scheduledata = {};
		var failure = function() {alert("There was a problem retreiving the schedule");};
		$.ajax({
			url:"https://www.riseofthefounders.com/?resource=schedule.json",
			contentType:"application/javascript",
			dataType:"json",
			type:"POST",
			processData:false,
			data:JSON.stringify({
				"ticket":token
			}),
			success:function(payload){
					scheduledata = $.map(payload, function(item) {
					return {
					id: item.ID,
					title: item.Name,
					start: item.StartDate.substr(0,10)+"T21:00:00.000",
					end: item.EndDate.substr(0,10)+"T14:00:00.000"
					};
				});
				var ww = $(window).width();
				var newview = (ww <= 768) ? 'listYear' : 'month';
				$('#calendar').fullCalendar({
					events: scheduledata,
					defaultView: newview,
					header: {
							left:   'title',
							center: '',
							right:  'today month listYear prev,next'
						},
					windowResize: function(view) {
						var ww = $(window).width();
						var newview = (ww <= 768) ? 'listYear' : 'month';
						//var currentView = $('#calendar').fullCalendar('getView');
						if(newview !== view.type){
							$('#calendar').fullCalendar('changeView',newview);
						}
					}
				});
			}
		}).fail(failure);
	});
}

function loadIntro(){
	"use strict";
	$("#mainbody").hide();
	var fadeArray = [];
	loadMainBody("./intro.html", function(data, status, xhr){
		var alltext = $("#mainbody");
		var writetext = alltext.find(".writeText");
		writetext.each(function(index){
			var l = fadeArray.push({tag: $(this)});
			fadeArray[l-1].values = jQuery.map($(this).text().split(''), function (letter) {
			return $('<span>' + letter + '</span>');});
			$(this).text("");
		});
		$("#mainbody").show();
	//		content= data;
	//			var fadeArray = jQuery.map(content.split(''), function (letter) {
	//			return $('<span>' + letter + '</span>');
	//    	});
		if(fadeArray.length){
			fadeTimeout = setTimeout(fadetext, 100, fadeArray, 0, 0);
		}
		//$("#mainbody").load("./about.html", getDowntimes);
	});
}

function fadetext(q, a, c){
	"use strict";
	var nextCharTime;
	var punct = ['.', ';', ',', '!', '?'];
	q[a].values[c].appendTo(q[a].tag).hide().fadeIn(1000);
	var thischar = q[a].values[c].text();
	c += 1;
	if(thischar && punct.indexOf(thischar) !== -1 ){
		nextCharTime = 700;}
	else{
		nextCharTime = 40;}
	if (c < q[a].values.length) { 
		fadeTimeout = setTimeout(fadetext, nextCharTime, q, a, c);}
	else if (a < q.length-1) { 
		c=0; 
		a += 1; 
		fadeTimeout = setTimeout(fadetext, nextCharTime, q, a, c);}
}

function loadAbout(){
	"use strict";
	//clearInterval(fadeTimeout);
	loadMainBody("./about.html");
}

function loadCharacterSheet(){
	"use strict";
	//clearInterval(fadeTimeout);
	loadMainBody("./charsheet.html", loadCharacterSheetData);
}

function changePassword(){
	"use strict";
	if($("#pcinput1").val() === $("#pcinput2").val() && $("#pcinput1").val().length > 4){
		var password = $("#pcinput1").val();
		$.ajax({
			url:"https://www.riseofthefounders.com/?c=execute&resource=RESETPASSWORD",
			contentType:"application/javascript",
			dataType:"json",
			type:"POST",
			processData:false,
			data:JSON.stringify({
				"ticket":token,"password":password
				}),
			success:function(){
				$("#passwordchange").modal("hide");
				alert("Password Changed. This may take a few minutes to take effect.");
				}
			});
	}
}

function loadCharacterSheetData(){
	"use strict";
	getResource("characters",function(payload){
				console.log(payload);
				var character = $(payload).filter(function(i, d){ 
				return d.CharacterId === charid;})[0]; 
		//payload[payload.length - 1];
				var charname = character.CharacterName;
				$("#charname").text(charname);
				var culture = character.Culture;
				$("#culture").text(culture);
				var zoul = character.Zoul;
				if(zoul){
					$("#hasZoulimg").show();
					$("#noZoulimg").hide();
					$("#zoul").text("Has Zoul");
				}
				else{
					$("#hasZoulimg").hide();
					$("#noZoulimg").show();
					$("#zoul").text("No Zoul");
				}
				var magic = character.Magic;
				if(magic){
					$("#magic").text(magic);
				}
				var template = character.TemplateName;
				if(template){
					$("#template").text(template + " | ");
				}
				var unspentXP = character.TotalUnspentXP;
				var totalXP = character.TotalEarnedXP;
				$("#xp").text(totalXP + "XP | " + unspentXP + "XP unspent");
				});
	getResource("charactersheets",function(payload){
		console.log(payload);
		payload = $(payload).filter(function(i, d){return d.charid === charid;});
		$("#skills-grid").DataTable({
			paging: false,
			info: false,
			data: payload,
			columns: [
				{data: "name"},
				{data: "cost",
					render: function(data, type, full){
					 return -1*data;
				 }},
				{data: "levels"},
				{data: "notes"}],
			order: [[1, "desc"]],
			responsive: {
			details: {
				type: 'column',
				target: 'tr'
			}}
		});
	});
	
}

/* Authentication */

function Auth(){
	"use strict";
	console.log(token);
	$.ajax({
		url:"https://www.riseofthefounders.com/?c=auth",
		contentType:"application/javascript",
		dataType:"json",
		type:"POST",
		processData:false,
		data:JSON.stringify({
			//"password":password
			//,"email":email
			"ticket":token
		}),
		success:function(payload){
			console.log(payload);
			//$("body").trigger("authentication",[payload]);
			activateUserMenu();
			loadDT();
			}
		}
	).fail(function(payload){
		console.log("failed = token = "+token);
		if(localStorage.getItem("Ticket")){
			localStorage.removeItem("Ticket");
			getTicketFromWebsite();
		}		
	});
}

/* AJAX */
function getResource(resource,callback,failure){
	"use strict";
	if(typeof failure !== "function"){
		failure = function(){
			console.log("default Failure");
		};
	}
	if(typeof callback !== "function"){
		callback = function(){
			console.log("default Callback");
		};
	}
	$.ajax({
		url:"https://www.riseofthefounders.com/?c=query&resource="+resource,
		contentType:"application/javascript",
		dataType:"json",
		type:"POST",
		processData:false,
		data:JSON.stringify({
			"ticket":token
		}),
		success:function(payload){
				callback(payload);
		}
	}).fail(failure);
}

function getDowntimes(){
	"use strict";
	getResource("downtimes",function(payload){
		console.log(payload);
		payload = $(payload).filter(function (i, d){return d.Charid === charid;});
		$("#downtime-grid").DataTable({
			"lengthMenu": [[3, 5, 10, -1], [3, 5, 10, "All"]],
			data: payload,
			columns: [
        		{data: "submission"},
				{data: "response"},
				{data: "submitted", 
				 type: "date",
				 render: function(data, type, full){
					 return new Date(data).toDateString();
				 }},
				{data: "Progress"}],
			order: [[2, "desc"]],
			responsive: {
            details: {
                type: 'column',
                target: 'tr'
      		}}
		});
	});
}


function logout(){
	"use strict";
	$.ajax({
		url:"https://www.riseofthefounders.com/?c=logout",
		contentType:"application/json",
		dataType:"json",
		type:"POST",
		processData:false,
		data:JSON.stringify({
				"ticket":token
		}),
		success:function(payload){
				localStorage.removeItem("Ticket");
				token = "";
				window.location.reload(); 
				}
			}
		).fail(function(){
			localStorage.removeItem("Ticket");
				token = "";
				window.location.reload(); 
		});
}

function AuthScreen(){
	"use strict";
	var email = $("#emailinput input")[0].value;
	var password = $("#passwordinput input")[0].value;
	console.log(token);
	$.ajax({ 
		url:"https://www.riseofthefounders.com/?c=auth",
		contentType:"application/json",
		dataType:"json",
		type:"POST",
		processData:false,
		data:JSON.stringify({
			"password":password,
			"email":email,
			"ticket":token
		}),
		success:function(payload){
			console.log(payload);
			localStorage.setItem("Ticket",token);
			activateUserMenu();
		}}).fail(function(payload){
		localStorage.removeItem("Ticket");
		token = $("ticket").text();
		alert("Incorrect Email or Password");
	});
	return false;
}

function listgroupselect(target){
	"use strict";
	$(target).siblings().removeClass("active");
	$(target).addClass("active");
}

function changeCharacter(){
	"use strict";
	var selectedID = $("#changechar .list-group .active").data("charid");
	charname = $("#changechar .list-group .active").text();
	$("#username").text(charname);
	localStorage.setItem("charID", selectedID);
	charid = selectedID;
	loadCharacterSheet();
	$("#changechar").modal("hide");
}

function loadDTForms(){
	"use strict";
	loadMainBody("./dtforms.html", dtFormSetup);
}

function dtFormSetup(){
	"use strict";
	$('#dtForms select').change(function () {
     	var optionSelected = $(this).find("option:selected");
		$("."+optionSelected.data("group")).hide();
     	$("#"+optionSelected.data("pair")).show();
 	});
}

function submitDowntimes(){
	"use strict";
	$.ajax({
		url:"https://www.riseofthefounders.com/?c=execute&resource=SUBMITDOWNTIMES",
		contentType:"application/javascript",
		type:"POST",
		processData:false,
		data:JSON.stringify({
			"ticket":token,
			"submission1":$("#submissiontext1").val(),
			"submission2":$("#submissiontext2").val(),
			"submission3":$("#submissiontext3").val(),
			"ritual1":$("#ritual1").val(),
			"ritual2":$("#ritual2").val(),
			"ritual3":$("#ritual3").val(),
			"researchtopic1":$("#topic1").val(),
			"researchtopic2":$("#topic2").val(),
			"researchtopic3":$("#topic3").val(),
			"type1":$("#dttype1").val(),
			"type2":$("#dttype2").val(),
			"type3":$("#dttype3").val(),
			"charid": charid,
			success:function(){ 
				alert("Downtimes Submitted");
				loadDT();
			}
			})
	}).fail( function() {alert("There was an error submitting your request.  Your login may have expired.  Please make a copy of your responses and refresh the page.");} );
}


function submitNotes(){
	"use strict";
	$.ajax({
		url:"https://wwww.riseofthefounders.com/?c=execute&resource=SUBMITCHARACTERNOTES",
		contentType:"application/javascript",
		type:"POST",
		processData:false,
		data:JSON.stringify({
			"ticket":token,
			"purchaserequest":$("#cppurchase").val(),
			"notes":$("#notestext").val(),
			"characterid": charid
		}),
		success:function(){
				alert("Notes Submitted");
			}
		}).fail( function() {alert("There was an error submitting your request.  Your login may have expired.  Please make a copy of your responses and refresh the page.");} );
}


